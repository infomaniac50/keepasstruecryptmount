Plugin for KeePass Password Safe - The Open-Source Password Manager
KeePass.TrueCryptMount
Copyright (C) 2010-2013 Patrick Schaller

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


References of other Authors and Artists.

-------------------------------Code----------------------------------------

File path regurlar expression
	Author:		FailedDev
			http://stackoverflow.com/users/880096/faileddev
	Source:		http://stackoverflow.com/a/7804916


-------------------------------Icons---------------------------------------

TrueCrypt-Icon: 
	Artist:		Unknown
	Source:		http://truecrypt.org

Registry-Icon:
	Artist:		TpdkDesign.net
	Iconset:	Refresh CI Icons
	License:	Free for non-commercial use.
	Source:		http://www.iconarchive.com

ReloadButton-Icon:
	Artist:		Deleket
	Iconset: 	Plastic Mini Icons
	License: 	CC Attribution-Noncommercial-No Derivate 3.0
	Source:		http://www.iconarchive.com