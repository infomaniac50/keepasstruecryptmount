﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassTrueCryptMount
{
    using System;
    using System.Windows.Forms;

    public static class FormExtensions
    {
        public static TForm WithStartPosition<TForm>(this TForm form, FormStartPosition startPosition)
            where TForm : Form
        {
            if (form == null)
            {
                throw new ArgumentNullException("form", "Form must not null.");
            }

            form.StartPosition = startPosition;
            return form;
        }

        public static TForm WithShowInTaskBar<TForm>(this TForm form, bool visible)
            where TForm : Form
        {
            if (form == null)
            {
                throw new ArgumentNullException("form", "Form must not null.");
            }

            form.ShowInTaskbar = visible;
            return form;
        }
    }
}
