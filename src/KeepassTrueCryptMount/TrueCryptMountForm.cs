﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  KeePassTrueCryptMount
  Copyright (C) 2010-2013 Patrick Schaller

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace KeePassTrueCryptMount
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    using KeePass.UI;
    using KeePassLib;
    using KeePassLib.Security;

    public partial class TrueCryptMountForm : Form
    {
        public TrueCryptMountForm()
        {
            InitializeComponent();

            this.driveComboBox.Items.Add(new AutoDriveLetterComboBoxItem());
            this.driveComboBox.Items.AddRange(LogicalDriveLetters.FreeDriveLetters.ToArray());

            this.backgroundCheckBox.CheckedChanged += this.OnBackgroundCheckBoxChanged;
        }

        public string ContainerVolume
        {
            get { return this.volumeTextBox.Text; }
        }

        public string KeyFiles
        {
            get { return this.keyFilesTextBox.Text; }
        }

        public string DriveLetter
        {
            get
            {
                return this.driveComboBox.SelectedItem is AutoDriveLetterComboBoxItem 
                                         ? string.Empty 
                                         : this.driveComboBox.SelectedItem as string;
            }
        }

        public bool OptionReadOnly
        {
            get { return this.readonlyCheckBox.Checked; }
            private set { this.readonlyCheckBox.Checked = value; }
        }

        public bool OptionRemovable
        {
            get { return this.removableCheckBox.Checked; }
            private set { this.removableCheckBox.Checked = value; }
        }

        public bool OptionSilent
        {
            get { return this.silentCheckBox.Checked; }
            private set { this.silentCheckBox.Checked = value; }
        }

        public bool HideForEntry
        {
            get { return this.hideDialogCheckBox.Checked; }
        }

        public bool ChangesApplied { get; set; }

        public TrueCryptMountForm ReadFrom(PwEntry entry)
        {
            // container
            var containerString = entry.Strings.GetSafe(EntryStrings.Volume);
            this.volumeTextBox.Text = containerString != null ? containerString.ReadString() : string.Empty;

            // KeyFiles
            var KeyFilesString = entry.Strings.GetSafe(EntryStrings.KeyFiles);
            this.keyFilesTextBox.Text = KeyFilesString != null ? KeyFilesString.ReadString() : string.Empty;

            // drive
            this.driveComboBox.SelectedIndex = 0;

            var driveString = entry.Strings.GetSafe(EntryStrings.DriveLetter);

            if (driveString != null)
            {
                var checkedValue = driveString.ReadString().EnsureIsDrive();

                if (!string.IsNullOrEmpty(checkedValue))
                {
                    this.driveComboBox.SelectedItem = checkedValue;
                }
            }
            
            // readonly
            this.OptionReadOnly = false;
            var readonlyString = entry.Strings.GetSafe(EntryStrings.Readonly);
            if (!readonlyString.IsEmpty)
            {
                bool readOnly;
                bool.TryParse(readonlyString.ReadString(), out readOnly);
                this.OptionReadOnly = readOnly;
            }

            // removable
            this.OptionRemovable = false;
            var removableString = entry.Strings.GetSafe(EntryStrings.Removable);
            if (!removableString.IsEmpty)
            {
                bool removable;
                bool.TryParse(removableString.ReadString(), out removable);
                this.OptionRemovable = removable;
            }

            // beep
            this.beepCheckBox.Checked = false;
            var beepString = entry.Strings.GetSafe(EntryStrings.Beep);
            if (!beepString.IsEmpty)
            {
                bool beep;
                bool.TryParse(beepString.ReadString(), out beep);
                this.beepCheckBox.Checked = beep;
            }

            // explorer
            this.explorerCheckBox.Checked = false;
            var explorerString = entry.Strings.GetSafe(EntryStrings.Explorer);
            if (!explorerString.IsEmpty)
            {
                bool explorer;
                bool.TryParse(explorerString.ReadString(), out explorer);
                this.explorerCheckBox.Checked = explorer;
            }

            // hide dialog
            this.hideDialogCheckBox.Checked = false;
            var hideDialogString = entry.Strings.GetSafe(EntryStrings.MountWithoutDialog);
            if (!hideDialogString.IsEmpty)
            {
                bool hideDialog;
                bool.TryParse(hideDialogString.ReadString(), out hideDialog);
                this.hideDialogCheckBox.Checked = hideDialog;
            }
            
            // silent
            this.OptionSilent = false;
            var silentString = entry.Strings.GetSafe(EntryStrings.Silent);
            if (!silentString.IsEmpty)
            {
                bool silent;
                bool.TryParse(silentString.ReadString(), out silent);
                this.OptionSilent = silent;
            }

            // ask for password
            this.askPasswordCheckBox.Checked = true;
            var askPasswordString = entry.Strings.GetSafe(EntryStrings.AskForPassword);
            if (!askPasswordString.IsEmpty)
            {
                bool askPassword;
                bool.TryParse(askPasswordString.ReadString(), out askPassword);
                this.askPasswordCheckBox.Checked = askPassword;
            }

            // background
            this.backgroundCheckBox.Checked = false;
            var backgroundString = entry.Strings.GetSafe(EntryStrings.Background);
            if (!backgroundString.IsEmpty)
            {
                bool background;
                bool.TryParse(backgroundString.ReadString(), out background);
                this.backgroundCheckBox.Checked = background;
            }
            else
            {
                this.backgroundCheckBox.Checked = true;
            }

            this.OnBackgroundCheckBoxChanged(this, EventArgs.Empty);

            this.btnApply.Enabled = false;

            return this;
        }

        public TrueCryptMountForm WriteTo(PwEntry entry)
        {
            entry.Strings.Set(EntryStrings.Volume, new ProtectedString(false, this.ContainerVolume));
            entry.Strings.Set(EntryStrings.KeyFiles, new ProtectedString(false, this.KeyFiles));
            entry.Strings.Set(EntryStrings.DriveLetter, new ProtectedString(false, this.DriveLetter));
            entry.Strings.Set(EntryStrings.Enabled, new ProtectedString(false, true.ToString()));
            entry.Strings.Set(EntryStrings.MountWithoutDialog, new ProtectedString(false, this.HideForEntry.ToString()));
            entry.Strings.Set(EntryStrings.Background, new ProtectedString(false, this.backgroundCheckBox.Checked.ToString()));
            entry.Strings.Set(EntryStrings.Silent, new ProtectedString(false, this.silentCheckBox.Checked.ToString()));
            entry.Strings.Set(EntryStrings.Removable, new ProtectedString(false, this.removableCheckBox.Checked.ToString()));
            entry.Strings.Set(EntryStrings.Readonly, new ProtectedString(false, this.readonlyCheckBox.Checked.ToString()));
            entry.Strings.Set(EntryStrings.Beep, new ProtectedString(false, this.beepCheckBox.Checked.ToString()));
            entry.Strings.Set(EntryStrings.Explorer, new ProtectedString(false, this.explorerCheckBox.Checked.ToString()));
            entry.Strings.Set(EntryStrings.AskForPassword, new ProtectedString(false, this.askPasswordCheckBox.Checked.ToString()));
            entry.AutoType.DefaultSequence = "{PASSWORD}{ENTER}";
            entry.AutoType.ObfuscationOptions = KeePassLib.Collections.AutoTypeObfuscationOptions.UseClipboard;

            return this;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Icon = Icon.FromHandle(Resources.TrueCryptNormal.GetHicon());

            this.bannerPanel.BackgroundImage = BannerFactory.CreateBanner(
                this.bannerPanel.Width,
                this.bannerPanel.Height,
                BannerStyle.Default,
                Resources.B48_TrueCrypt,
                LanguageTexts.BannerPluginTitleText,
                LanguageTexts.BannerMountFormDescriptionText);

            this.btnApply.Click += this.OnApplyButtonClicked;

            GlobalWindowManager.AddWindow(this);
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            this.btnApply.Click -= this.OnApplyButtonClicked;

            GlobalWindowManager.RemoveWindow(this);

            base.OnFormClosed(e);
        }

        private void OnAnyOptionChanged(object sender, EventArgs e)
        {
            this.btnApply.Enabled = true;
        }

        private void OnBackgroundCheckBoxChanged(object sender, EventArgs e)
        {
            var backgroundChecked = this.backgroundCheckBox.Checked;

            this.silentCheckBox.Enabled = backgroundChecked;
            this.silentCheckBox.Checked = !backgroundChecked ? false : this.silentCheckBox.Checked;

            this.askPasswordCheckBox.Enabled = backgroundChecked;
            this.askPasswordCheckBox.Checked = !backgroundChecked ? true : this.askPasswordCheckBox.Checked;
        }

        private void OnApplyButtonClicked(object sender, EventArgs e)
        {
            this.ChangesApplied = true;
            this.btnApply.Enabled = false;
        }

        private void OnFileOpenDialogButtonClicked(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            if (sender.Equals(button2))
            {
                dialog.Multiselect = true;
                if (dialog.ShowDialog(this) == DialogResult.OK && dialog.CheckFileExists)
                {
                    var keyFiles = new StringBuilder();
                    foreach (string i in dialog.FileNames)
                    {
                        keyFiles.Append(i + ";");
                    }
                    this.keyFilesTextBox.Text = keyFiles.ToString();
                }
            }else if(sender.Equals(button1))
            {
                dialog.Multiselect = false;
            
                if (dialog.ShowDialog(this) == DialogResult.OK && dialog.CheckFileExists)
                {
                    this.volumeTextBox.Text = dialog.FileNames.ToString();
                }
            }
        }

        private void OnReloadDrivesButtonClicked(object sender, EventArgs e)
        {
            var selectedItem = this.driveComboBox.SelectedItem;

            this.driveComboBox.Items.Clear();
            this.driveComboBox.Items.Add(new AutoDriveLetterComboBoxItem());
            this.driveComboBox.Items.AddRange(LogicalDriveLetters.FreeDriveLetters.ToArray());

            if (selectedItem == null || selectedItem is AutoDriveLetterComboBoxItem)
            {
                this.driveComboBox.SelectedIndex = 0;
            }
            else
            {
                this.driveComboBox.SelectedItem = selectedItem;
            }
        }
    }
}
